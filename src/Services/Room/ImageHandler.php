<?php


namespace App\Services\Room;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Flex\Path;

class ImageHandler
{
    private $path;

    public function __construct($path)
    {
        $this->path = $path.'/public/images/room';
    }

    public function handle(Image $image)
    {
        //récupère le fichier soumis
        /** @var UploadedFile $file */
        $file = $image->getFile();
        $name = $this->createName($file);

        //Déplace le fichier
        $image->setName($name);
        $file->move($this->path, $name);
    }

    private function createName(UploadedFile $file): string
    {
        return md5(uniqid()). $file->getClientOriginalName().'.'.$file->guessExtension();
    }
}