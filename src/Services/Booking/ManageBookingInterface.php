<?php


namespace App\Services\Booking;


use App\Entity\User;
;

interface ManageBookingInterface
{
    public function displayRoomsAvailable(User $user, \DateTime $dateStart, int $duration): iterable ;
}