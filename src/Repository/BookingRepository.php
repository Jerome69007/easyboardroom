<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }


   /* public function hasBooked(int $idRoom,\DateTime $dateStart,\DateTime $hourStart) {

        return $this->createQueryBuilder('b')

            ->where('b.room = :idRoom')
            ->setParameter('idRoom', $idRoom)



            ;
    }*/

    public function findBookingsByRoomAndDate(int $roomId, \DateTime $dateStart, \DateTime $hourStart) {

        return $this->createQueryBuilder('b')

             # Get bookings from roomId
            ->where('b.room = :roomId')
            ->setParameter('roomId', $roomId)

            # Filter by date
            ->andWhere('b.dateStart = :dateStart')
            ->setParameter('dateStart', $dateStart)

            # Filter by hourStart
            ->andWhere('b.HourStart = :hourStart')
            ->setParameter('hourStart', $hourStart->format('H:i'))

            ->getQuery()
            ->getResult()
        ;

    }

    // /**
    //  * @return Booking[] Returns an array of Booking objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Booking
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
