<?php

namespace App\Controller;

use App\Entity\Room;
use App\Form\RoomType;
use App\Repository\RoomRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\DependencyInjection\Security\UserProvider\EntityFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RoomController extends AbstractController
{
    /**
     * @Route("/room", name="room_index", defaults={"id": null}, requirements={"id": "\d+"})
     * @return Response
     */
    public function index()
    {
        return $this->render('room/index.html.twig',
            [
            'controller_name' => 'RoomController',
            ]);
    }

    /**
     * @Route("/edit/{id}", name="room_edit", defaults={"id": null}, requirements={"id": "\d+"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function edit(
        Request $request,
        EntityManagerInterface $em
        )
    {
        $room = new Room();
        $form = $this->createForm(RoomType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em->persist($room);
                $em->flush();

                $this->addFlash('success', "La salle est enregistrée");

                return $this->redirectToRoute('room_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'room/edit_room.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/suppression/{id}", name="room_delete")
     * @param EntityManagerInterface $em
     * @param Room $room
     * @return RedirectResponse
     */
    public function delete(
        EntityManagerInterface $em,
        Room $room
        )
        {
        $em->remove($room);
        $em->flush();

        $this->addFlash('success', "La salle est supprimée");

        return $this->redirectToRoute('room_index');
    }

}
