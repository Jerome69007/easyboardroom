<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Room;
use App\Form\AvailabilityType;
use App\Repository\BookingRepository;
use App\Repository\RoomRepository;
use App\Repository\TimetableRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class BookingController
 * @Route("/booking", name="booking")
 * @package App\Controller
 */
class BookingController extends AbstractController
{
    /**
     * @Route("/create", name="_create", methods={"GET"})
     */
    public function create()
    {
        $form = $this->createForm(AvailabilityType::class,
            [
                'dateStart' => new \DateTime()
            ],
            [
                'action' => $this->generateUrl('booking_availability'),
                'method' => 'GET',
            ]);

        return $this->render('booking/manage_booking.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/availability",name="_availability", methods={"GET"})
     * @param Request $request
     * @param TimetableRepository $timetableRepository
     * @param RoomRepository $roomRepository
     * @return Response
     */
    public function createManageBooking(
        Request $request,
        TimetableRepository $timetableRepository,
        RoomRepository $roomRepository)
    {


            $availability = $request->query->get('availability');

            $date_start = $availability['dateStart'];

            $duration = $availability['duration'];

        $start_date_converted = \DateTime::createFromFormat('d-m-Y', $date_start);
        $start_date_converted->setTime(00, 00, 00);

        return $this->render('booking/display_planing_for_booking.html.twig', [
            'planing_date' => $start_date_converted,
            'timestamp' => $start_date_converted->getTimestamp(),
            'duration' => $duration,
            'rooms' => $roomRepository->findAll(),
            'timetable' => $timetableRepository->findBy([], ['hourStart' => 'ASC']),

        ]);

    }

    /**
     * @Route("/display/{start}",defaults={"start": null },name="_display")
     * @param RoomRepository $roomRepository
     * @param TimetableRepository $timetableRepository
     * @return Response
     * @throws \Exception
     */
    public function displayPlanningOfDay(
        RoomRepository $roomRepository,
        TimetableRepository $timetableRepository,$start)
    {

        $dateOfDay = new \DateTime();

        if (is_null($start)) {

            $dateOfDay->format('d-m-Y');
            $dateOfDay->setTime(00, 00, 00);


        } else {
            $convertDate =  date('d-m-Y',$start);
            $dateOfDay = \DateTime::createFromFormat('d-m-Y', $convertDate);

        }

        $dateOfDay->setTime(00, 00, 00);



        return $this->render('booking/planning_of_day.html.twig',[
            'display' => true,
            'planing_date' => $dateOfDay,

            'list_rooms' => $roomRepository->findAll(),
            'timetable' => $timetableRepository->findBy([], ['hourStart' => 'ASC'])
        ]);
    }


    /**
     *
     * @Route("/book/{id}-{dateStart}-{hourStart}-{duration}.html", name="_book", methods={"GET"})
     * @param Room $room
     * @param $dateStart
     * @param $hourStart
     * @param $duration
     * @param BookingRepository $bookingRepository
     * @return Response
     * @throws \Exception
     */
    public function bookRoom(
        Room $room,
        $dateStart,
        $hourStart,
        $duration,
        BookingRepository $bookingRepository)
    {

        $start = new \DateTime();
        $start->setTimestamp($dateStart);
        $start->setTime(00, 00, 00);

        $roomId =  $room->getId();


        $hour = \DateTime::createFromFormat('H:i', $hourStart);

        // check if booking is ever made (case refresh browser)
        $checkHasBooked = $bookingRepository->findBookingsByRoomAndDate($roomId,$start,$hour);


        if (!empty($checkHasBooked) )
        {
            $this->addFlash('danger',"Votre réservation est déjà effectuée");


            return $this->redirectToRoute('booking_display');

        }

        $user = $this->getUser();


        $hour->sub(new \DateInterval('PT1H'));

        for ($i = 1; $i <= $duration; $i++) {

            $booking = new Booking();


            $booking->setUser($user)
                ->setStatus('todo')
                ->setRoom($room)
                ->setDateStart($start)
                ->setHourStart($hour)
                ->setDuration(1)
            ;



            $booking->setHourStart($hour->add(new \DateInterval('PT1H')));
            $room->addBooking($booking);


            # TODO : A réfléchir double checker la disponibilité. Et si non dispo. Liste d'attente ?
            # FIXME Workflow process here

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($booking);

            $manager->flush();
        }

        # FIXME Change process here

        $userId = $this->getUser()->getId();
        $deleted = false;


        return $this->redirectToRoute('user_booking',[
            'id'=>$userId,
            'deleted' =>$deleted
        ]);


    }


}
