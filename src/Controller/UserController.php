<?php

namespace App\Controller;


use App\Entity\Booking;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();


        return $this->render('user/login.html.twig',[

            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/deconnexion" ,name="logout")
     */
    public function logout(){}


    /**
     * @param User $user
     * @param $deleted
     * @return Response
     * @Route("/userBooking/{id}/{deleted}", defaults={"deleted" : null}, name="user_booking")
     */
    public function userBooking(User $user, $deleted)
    {
       #FIXME deleted value from booking is null ??

        if (!empty($deleted) and $deleted == true)
        {

            $this->addFlash('success', "La réservation est supprimée");
        }
        if (!empty($deleted) and $deleted == false)
        {
            $this->addFlash('success', "La réservation est enregistrée");
        }

        $liste = $user->getBooking();



        return $this->render(
            'user/user_booking.html.twig',
            [
                'liste' => $liste,
                'user_booking' => $user

            ]
        );
    }

    /**
     * @param ObjectManager $manager
     * @param Booking $booking
     * @param $admin
     * @return RedirectResponse
     * @Route("/deleteUserBooking/{id}/{admin}",defaults={"admin" : null },requirements={"admin" : "\d+" }, name="delete_user_booking")
     */
    public function deleteUserBooking(ObjectManager $manager, Booking $booking,$admin)
    {
        $user = $this->getUser();


        $manager->remove($booking);
        $manager->flush();

        $deleted = true;

        // If delete user booking come from admin
        if (!null == $admin) {
            $lastNameUser = $booking->getUser()->getLastname();
            $firstNameUser = $booking->getUser()->getFirstname();
            $this->addFlash('success',"La réservation de $firstNameUser $lastNameUser à bien été supprimée");
            return $this->redirectToRoute('admin_edit_user',[

            ]);
        }

        return $this->redirectToRoute('user_booking', [
            'id'=> $user->getId(),
            'deleted' =>$deleted

        ]);

    }

}



