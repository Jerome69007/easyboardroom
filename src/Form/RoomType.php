<?php

namespace App\Form;


use App\Entity\Room;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',
                TextType::class,
                [
                    'label' => 'Name'
                ])
            ->add('capacity',
                TextType::class,
                [
                    'label' => 'Capacité'
                ])
            ->add('facilities',
                TextType::class,
                [
                    'label' => 'Equipement'
                ])
            ->add('image',
                FileType::class,
                [
                    'label' => 'Photo',
                    #'required' => false
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
            'path' => null,
        ]);
    }
}
