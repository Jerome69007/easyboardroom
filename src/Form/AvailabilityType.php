<?php


namespace App\Form;


use App\Entity\Booking;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AvailabilityType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateStart',
                DateType::class,
                [
                    //'widget' => 'choice',
                    'widget' => 'single_text',
                    'label' =>'Date de réservation souhaitée',
                    'html5' => false,
                    'format' => 'dd-MM-yyyy',
                   'attr' => [
                       'class' => 'datepicker'
                   ]
                   // 'label' => 'date de début'
                ])
            ->add('duration',
                ChoiceType::class, [
                    "choices"  => [
                        '1 heure' => 1,
                        '2 heures' => 2,
                        '3 heures' => 3,
                        '4 heures' => 4,
                        '5 heures' => 5,
                        '6 heures' => 6,
                        '7 heures' => 7,
                        '8 heures' => 8,
                        '9 heures' => 9,
                        '10 heures' => 10,
                        '11 heures' => 11
                    ],
                ])
            ->add('submit', SubmitType::class, [
                'label' => 'Vérifier les disponibilités'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', null);
    }
}