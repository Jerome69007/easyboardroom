-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 12 juil. 2019 à 09:51
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `easyboardroom`
--

-- --------------------------------------------------------

--
-- Structure de la table `booking`
--

DROP TABLE IF EXISTS `booking`;
CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `booking_date` datetime NOT NULL,
  `date_start` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hour_start` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E00CEDDE54177093` (`room_id`),
  KEY `IDX_E00CEDDEA76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `booking`
--

INSERT INTO `booking` (`id`, `room_id`, `user_id`, `booking_date`, `date_start`, `status`, `hour_start`) VALUES
(1, 1, 1, '2019-07-11 00:00:00', '2019-07-11 00:00:00', '', '14:00:00'),
(2, 1, 1, '2019-07-11 00:00:00', '2019-07-11 00:00:00', '', '15:00:00'),
(3, 2, 1, '2019-07-11 00:00:00', '2019-07-11 00:00:00', '', '17:00:00'),
(4, 2, 1, '2019-07-11 00:00:00', '2019-07-11 00:00:00', '', '18:00:00'),
(9, 2, 1, '2019-07-11 00:00:00', '2019-07-11 00:00:00', '', '09:00:00'),
(10, 2, 1, '2019-07-12 00:00:00', '2019-07-12 00:00:00', '', '09:00:00'),
(11, 5, 2, '2019-07-12 00:00:00', '2019-07-12 00:00:00', '', '14:00:00'),
(12, 5, 2, '2019-07-12 00:00:00', '2019-07-12 00:00:00', '', '15:00:00'),
(13, 5, 2, '2019-07-12 00:00:00', '2019-07-12 00:00:00', '', '16:00:00'),
(21, 3, 1, '2019-07-12 00:00:00', '2019-07-12 00:00:00', 'todo', '10:00:00'),
(22, 3, 1, '2019-07-12 00:00:00', '2019-07-12 00:00:00', 'todo', '11:00:00'),
(23, 3, 1, '2019-07-12 00:00:00', '2019-07-12 00:00:00', 'todo', '12:00:00'),
(24, 3, 1, '2019-07-12 00:00:00', '2019-07-12 00:00:00', 'todo', '13:00:00'),
(25, 3, 1, '2019-07-12 00:00:00', '2019-07-12 00:00:00', 'todo', '18:00:00'),
(26, 1, 1, '2019-07-12 00:00:00', '2019-07-12 00:00:00', 'todo', '12:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190711101155', '2019-07-11 10:11:59'),
('20190711112907', '2019-07-11 11:29:17'),
('20190711113055', '2019-07-11 11:30:57'),
('20190711114034', '2019-07-11 11:40:37'),
('20190712093521', '2019-07-12 09:35:27');

-- --------------------------------------------------------

--
-- Structure de la table `room`
--

DROP TABLE IF EXISTS `room`;
CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) DEFAULT NULL,
  `capacity` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `facilities` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `available` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_729F519B3DA5256D` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `room`
--

INSERT INTO `room` (`id`, `image_id`, `capacity`, `name`, `facilities`, `available`) VALUES
(1, NULL, 10, 'Le Mordor', 's:17:\"Vidéo Projecteur\";', 1),
(2, NULL, 20, 'Terre du Milieu', 's:14:\"Café, un Lama\";', 1),
(3, NULL, 5, 'Isengard', 's:17:\"Vidéo Projecteur\";', 1),
(4, NULL, 30, 'Minas Tirith', 's:17:\"Vidéo Projecteur\";', 1),
(5, NULL, 30, 'Le Rohan', 's:17:\"Vidéo Projecteur\";', 1),
(6, NULL, 30, 'Le Pays de Bree', 's:17:\"Vidéo Projecteur\";', 1);

-- --------------------------------------------------------

--
-- Structure de la table `timetable`
--

DROP TABLE IF EXISTS `timetable`;
CREATE TABLE IF NOT EXISTS `timetable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hour_start` time NOT NULL,
  `hour_end` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `timetable`
--

INSERT INTO `timetable` (`id`, `hour_start`, `hour_end`) VALUES
(2, '08:00:00', '09:00:00'),
(3, '09:00:00', '10:00:00'),
(4, '11:00:00', '12:00:00'),
(5, '13:00:00', '14:00:00'),
(6, '15:00:00', '16:00:00'),
(7, '17:00:00', '18:00:00'),
(8, '18:00:00', '19:00:00'),
(9, '10:00:00', '11:00:00'),
(10, '12:00:00', '13:00:00'),
(11, '16:00:00', '17:00:00'),
(12, '14:00:00', '15:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `lastname`, `firstname`, `email`, `password`, `roles`) VALUES
(1, 'LIEGEARD', 'Hugo', 'hugo@livretoo.fr', '$argon2i$v=19$m=65536,t=6,p=1$eEVtQ0RVbXVLZ1NBZi9QWA$5sqgC5zF1rA8rpSOZpuUooo2BhZ/8LapkHpF+EcV0KA', 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}'),
(2, 'VERGER', 'Sébastien', 'seb@livretoo.fr', '$2y$12$f1C.3rFqT7kQ3KR5xZj/.e84Jg2XVXxXYccv3HT.xU9oHOVQTNx0q', 'a:1:{i:0;s:9:\"ROLE_USER\";}');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `FK_E00CEDDE54177093` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `FK_E00CEDDEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `FK_729F519B3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
